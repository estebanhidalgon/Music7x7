import React from "react";
import ReactDOM from "react-dom";
import GoogleLogin from "react-google-login";

import "./styles.css";

const responseGoogle = response => {
  console.log(response);
};

function App() {
  return (
    <GoogleLogin
      scope="https://www.googleapis.com/auth/drive"
      clientId="906534007358-atde1pmvq52d6nnqrisjv6b72drn22oe.apps.googleusercontent.com"
      buttonText="Iniciar Seción"
      onSuccess={responseGoogle}
      onFailure={responseGoogle}
      cookiePolicy={"single_host_origin"}
    />
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
